PGDMP     :                    v        
   BlogPoster    10.4    10.4 *               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16430 
   BlogPoster    DATABASE     �   CREATE DATABASE "BlogPoster" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "BlogPoster";
             vproh    false                       0    0    DATABASE "BlogPoster"    COMMENT     /   COMMENT ON DATABASE "BlogPoster" IS 'Demo #1';
                  vproh    false    2839                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16485    post    TABLE     \  CREATE TABLE public.post (
    post_id integer NOT NULL,
    title character varying(100) NOT NULL,
    description text,
    content text NOT NULL,
    image_url text,
    date_of_create date NOT NULL,
    date_of_publish date,
    date_of_update date,
    time_of_update time(3) without time zone,
    tag_id integer[],
    users_id integer[]
);
    DROP TABLE public.post;
       public         vproh    false    3                       0    0 
   TABLE post    COMMENT     <   COMMENT ON TABLE public.post IS 'contains info about post';
            public       vproh    false    198            �            1259    16489    post_post_id_seq    SEQUENCE     �   CREATE SEQUENCE public.post_post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.post_post_id_seq;
       public       vproh    false    198    3                       0    0    post_post_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.post_post_id_seq OWNED BY public.post.post_id;
            public       vproh    false    199            �            1259    16500    roles    TABLE     e   CREATE TABLE public.roles (
    role_id integer NOT NULL,
    name character varying(30) NOT NULL
);
    DROP TABLE public.roles;
       public         vproh    false    3                       0    0    TABLE roles    COMMENT     8   COMMENT ON TABLE public.roles IS 'contains blog roles';
            public       vproh    false    200            �            1259    16503    roles_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.roles_role_id_seq;
       public       vproh    false    3    200                       0    0    roles_role_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.roles_role_id_seq OWNED BY public.roles.role_id;
            public       vproh    false    201            �            1259    16513    tags    TABLE     V   CREATE TABLE public.tags (
    tag_id integer NOT NULL,
    tag_name text NOT NULL
);
    DROP TABLE public.tags;
       public         vproh    false    3                       0    0 
   TABLE tags    COMMENT     U   COMMENT ON TABLE public.tags IS 'contains tag for post. Cannot be modified by user';
            public       vproh    false    203            �            1259    16511    tags_tag_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tags_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.tags_tag_id_seq;
       public       vproh    false    3    203                        0    0    tags_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.tags_tag_id_seq OWNED BY public.tags.tag_id;
            public       vproh    false    202            �            1259    16476    user    TABLE     '  CREATE TABLE public."user" (
    user_id integer NOT NULL,
    name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    social_url text,
    is_blocked boolean NOT NULL,
    role_id integer DEFAULT 1 NOT NULL
);
    DROP TABLE public."user";
       public         vproh    false    3            !           0    0    TABLE "user"    COMMENT     ?   COMMENT ON TABLE public."user" IS 'contains user information';
            public       vproh    false    197            �            1259    16474    user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.user_user_id_seq;
       public       vproh    false    3    197            "           0    0    user_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.user_user_id_seq OWNED BY public."user".user_id;
            public       vproh    false    196            �
           2604    16491    post post_id    DEFAULT     l   ALTER TABLE ONLY public.post ALTER COLUMN post_id SET DEFAULT nextval('public.post_post_id_seq'::regclass);
 ;   ALTER TABLE public.post ALTER COLUMN post_id DROP DEFAULT;
       public       vproh    false    199    198            �
           2604    16505    roles role_id    DEFAULT     n   ALTER TABLE ONLY public.roles ALTER COLUMN role_id SET DEFAULT nextval('public.roles_role_id_seq'::regclass);
 <   ALTER TABLE public.roles ALTER COLUMN role_id DROP DEFAULT;
       public       vproh    false    201    200            �
           2604    16516    tags tag_id    DEFAULT     j   ALTER TABLE ONLY public.tags ALTER COLUMN tag_id SET DEFAULT nextval('public.tags_tag_id_seq'::regclass);
 :   ALTER TABLE public.tags ALTER COLUMN tag_id DROP DEFAULT;
       public       vproh    false    202    203    203            �
           2604    16479    user user_id    DEFAULT     n   ALTER TABLE ONLY public."user" ALTER COLUMN user_id SET DEFAULT nextval('public.user_user_id_seq'::regclass);
 =   ALTER TABLE public."user" ALTER COLUMN user_id DROP DEFAULT;
       public       vproh    false    196    197    197                      0    16485    post 
   TABLE DATA                     public       vproh    false    198   �'                 0    16500    roles 
   TABLE DATA                     public       vproh    false    200   (.                 0    16513    tags 
   TABLE DATA                     public       vproh    false    203   �.                 0    16476    user 
   TABLE DATA                     public       vproh    false    197   /       #           0    0    post_post_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.post_post_id_seq', 59, true);
            public       vproh    false    199            $           0    0    roles_role_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.roles_role_id_seq', 2, true);
            public       vproh    false    201            %           0    0    tags_tag_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.tags_tag_id_seq', 17, true);
            public       vproh    false    202            &           0    0    user_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.user_user_id_seq', 18, true);
            public       vproh    false    196            �
           2606    16499    post post_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (post_id);
 8   ALTER TABLE ONLY public.post DROP CONSTRAINT post_pkey;
       public         vproh    false    198            �
           2606    16510    roles roles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (role_id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public         vproh    false    200            �
           2606    16521    tags tags_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (tag_id);
 8   ALTER TABLE ONLY public.tags DROP CONSTRAINT tags_pkey;
       public         vproh    false    203            �
           2606    16484    user user_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public         vproh    false    197            �
           2606    16546    user user_fk_role    FK CONSTRAINT     w   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_fk_role FOREIGN KEY (role_id) REFERENCES public.roles(role_id);
 =   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_fk_role;
       public       vproh    false    2701    200    197                 x��Y]o�F}��0�@�I�I�-�ؤ�7[4����%g�k�ag�r��@ľ�����^Rv��v���B@E��q�W�s�W�_~w�.^]��u}�U5�����Q|ʼ�ZL����ʴSV�֋�O�j�oz��x���7�x��=��V��;>t��mC=,�;a�=g��������Y���S�+��ϥ��̍c'����!�t��۵t�Z�G�fg�f#�u�pw��2βh�� �WEPfU�e�|�岊�� y�;!��x�{+f][�tq�A�Q��Wח����i�#u|e?�>?�\EzrJ�^�����r������c����{f)+
Q���� �r xDUQ�\�<��9mnW��i:���4����T��/Znl^U�A��7�W�`�4�Z�R�j͌$�����L�v�4��޲j��Fk���+��y���8�`�����?Кݮ���f��0xXv�<��ط�0�X�+�٭�+\y���K&�h�>ǜ��-ñ�f��xk��̯k���"���xƮ��o-t!���͗wsS$�㉒?0�11���Cn����B7���1!�ժ5��J��߬���LN&��s5҈s#��!��@�V�Cc) Ǵ���Z=�<�l�/?����rClh$�b����`��C.��1ba��8#ΆPz�;������xMG���È���i:��Ta�����o!b	K� B�ľ`1+\��à�Z�H;���G`��5Ǳ����;#G�>�m�]��gx���c?�@�h���RjE�2�5��i��T��#�S�aO���:mMSR$/��?GH��a�s��R��='�{Ν48"r
��mu�G��^U�[7�O�:���<���N||$a��S@�]��s��q ����y\?��+�i�����˧���ϻ��Z<�M?��3Z,����ncw%O	�T���2���Ռ�^{��")!��|I$�A�.P����e��{z�'[n1fj�m��6S�e*�)#�2�� tF��M'o:�?q��j׀�?�����NY.�x�~�	�y
����M�L��$?]p||.8�{]�L�?u�g�����O{ֲ���][��9�N?ئ���ң�n��)b�s˼D��0չ�a�e��C#���d��Q��U����Vx�47L��5oO^���L��$ޣ��=�d������%@�����G��]�l�	��d�}�"�Em��쭰��﵆�2���	a�RÜ���h�H�#*s2�������9�G�,1ƫ�	uV�D�v��/6F��'���`�pN�TJ�;�0�	���cCH(���v�^��D�EO\"��@Tد�;���ӞL�K��$�p٪׸a�?$r=�!a�jc4"I!#n�m�̎O��]tG_t�Kᡊ갥p(T+�C:`i�aK�P������x�',�p�}���sG�4[�.����R<=2����|��])O���������p�ε�sN�K��ď2ʳ"	�a��-W "��,ϓ�(ʼ̊��X��O��b9>D����O#D�oB_��茻�������P."�c�b�	��C�Ţ"��DFQ��9Zܻ��rCa�e2>��wL�d�+���(         R   x���v
Q���W((M��L�+��I-V� Q�):
y����
a�>���
�:
�ũE��\��j6jNL������ �]%z         �   x���v
Q���W((M��L�+IL/V� ��):
 :/17US!��'�5XA�PGA=/���(U]Ӛ˓t���e����=+�,�L�&@��%�yd`
4�<5�L���K-IL'� C�	����� 	�         �  x�͒O��0��~
21Q��p���Q��PV/�B�R�HAe>����������ۧ�>yk;��#�l'p��>�(^j
�����<���\
h��Y����
@鍔�%�.EtHI�!�%I!k1�6��z�s}I湞}"e��7�(���}=�m��d-�ʠ+�ݡ����_y#A��K�Dػ	��&* Gmf�O����]���E��y{�Y/�<w)e�_:�_U��T�u٠�V��m���맺o�eb٫љH^R3���9�;q�*�{���������X�oݪ������ }�
ƃV�+	>稢��O�M�;"���|�B�̇�	����f���I)��М�� ƶF�È�v�)���_9V�i��fN-˻�jV�~�*�s�e�d�XUq�&Q3�բ\iԵ���3�̴����-�     