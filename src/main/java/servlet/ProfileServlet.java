package servlet;

import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.AddPostServlet.INFO_MESSAGE_ATTR;
import static servlet.ExploreServlet.CURRENT_USER_ATTR;
import static servlet.LoginServlet.USER_SESSION_ATTR;

@WebServlet(urlPatterns = {"/profile"})
public class ProfileServlet extends HttpServlet {
    static final String UPDATE_USER_ERROR_ATTR = "updateUserError";

    public static final String USER_CHANGE_PARAM = "change";
    public static final String PROFILE_SERVLET = "profile";
    private static final String PROFILE_JSP = "jsp/profile.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User currentUser = (User) request.getSession().getAttribute(USER_SESSION_ATTR);
        request.setAttribute(CURRENT_USER_ATTR, currentUser);
        request.setAttribute(INFO_MESSAGE_ATTR, request.getSession().getAttribute(INFO_MESSAGE_ATTR));
        request.setAttribute(UPDATE_USER_ERROR_ATTR, request.getSession().getAttribute(UPDATE_USER_ERROR_ATTR));
        request.getSession().removeAttribute(INFO_MESSAGE_ATTR);
        request.getSession().removeAttribute(UPDATE_USER_ERROR_ATTR);
        request.getRequestDispatcher(PROFILE_JSP).forward(request, response);
    }
}
