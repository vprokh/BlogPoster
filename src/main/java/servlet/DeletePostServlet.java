package servlet;

import models.User;
import org.apache.log4j.Logger;
import services.PostService;
import services.UserService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.AddPostServlet.INFO_MESSAGE_ATTR;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.SinglePostServlet.POST_ID_PARAM;
import static servlet.UserPostServlet.POST_TYPE_PARAM;
import static servlet.UserPostServlet.USER_POSTS_SERVLET;
import static servlet.admin.PostsServlet.POSTS_SERVLET_ADMIN;

@WebServlet(urlPatterns = {"/deletePost"})
public class DeletePostServlet extends HttpServlet {
    public static final String DELETE_POST_SERVLET = "deletePost";
    private static final String SUCCESSFULLY_DELETED_MESSAGE = "Post has been successfully deleted!";
    private static final Logger LOG = Logger.getLogger(DeletePostServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PostService postService = new PostService();
        UserService userService = new UserService();
        String postType = request.getParameter(POST_TYPE_PARAM);
        User currentUser = (User) request.getSession().getAttribute(USER_SESSION_ATTR);
        int postId = Integer.parseInt(request.getParameter(POST_ID_PARAM));

        try {
            postService.deletePostById(postId);
            if (userService.isUserAdmin(currentUser.getEmail()))
                response.sendRedirect(POSTS_SERVLET_ADMIN);
            else if (userService.isRoleUser(currentUser.getEmail())) {
                request.getSession().setAttribute(INFO_MESSAGE_ATTR, SUCCESSFULLY_DELETED_MESSAGE);
                response.sendRedirect(USER_POSTS_SERVLET + "?" + POST_TYPE_PARAM + "=" + postType);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
