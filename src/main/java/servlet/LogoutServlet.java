package servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static servlet.ExploreServlet.EXPLORE_SERVLET;
import static servlet.LoginServlet.USER_SESSION_ATTR;

@WebServlet(urlPatterns = {"/logout"})
public class  LogoutServlet extends HttpServlet {
    public static final String LOGOUT_SERVLET = "logout";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.removeAttribute(USER_SESSION_ATTR);
        session.invalidate();

        response.sendRedirect(EXPLORE_SERVLET);
    }
}
