package servlet.admin;

import org.apache.log4j.Logger;
import services.UserService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.admin.UsersServlet.USERS_SERVLET_ADMIN;

@WebServlet(urlPatterns = {"/banUser"})
public class BanUserServlet extends HttpServlet {
    public static final String BAN_USER_SERVLET_ADMIN = "banUser";
    static final String USER_ID_PARAM = "userId";
    private static final String BAN_PARAM = "ban";
    private static final Logger LOG = Logger.getLogger(BanUserServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            boolean ban = Boolean.parseBoolean(request.getParameter(BAN_PARAM));
            int userId = Integer.parseInt(request.getParameter(USER_ID_PARAM));
            new UserService().banUserById(userId, ban);
            response.sendRedirect(USERS_SERVLET_ADMIN);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
