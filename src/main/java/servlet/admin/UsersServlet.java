package servlet.admin;

import models.User;
import org.apache.log4j.Logger;
import services.UserService;
import servlet.LoginServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.ExploreServlet.CURRENT_USER_ATTR;

@WebServlet(urlPatterns = {"/users"})
public class UsersServlet extends HttpServlet {
    public static final String USERS_SERVLET_ADMIN = "users";
    private static final String USERS_ATTR = "users";
    private static final String USERS_JSP_ADMIN = "jsp/admin/users.jsp";
    private static final Logger LOG = Logger.getLogger(UsersServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = new UserService();
        User currentUser = (User) request.getSession().getAttribute(LoginServlet.USER_SESSION_ATTR);

        try {
            List<User> users = userService.getAllUsers(currentUser.getUserId()); // without currentUser
            request.setAttribute(USERS_ATTR, users);
            request.setAttribute(CURRENT_USER_ATTR, currentUser);
            request.getRequestDispatcher(USERS_JSP_ADMIN).forward(request, response);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
