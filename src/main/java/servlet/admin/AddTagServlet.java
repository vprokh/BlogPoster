package servlet.admin;

import org.apache.log4j.Logger;
import services.TagService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.admin.TagsServlet.TAGS_SERVLET_ADMIN;

@WebServlet(urlPatterns = {"/addTag"})
public class AddTagServlet extends HttpServlet {
    public static final String ADD_TAG_SERVLET_ADMIN = "addTag";
    private static final String TAG_NAMES_PARAM = "tagNames";
    private static final Logger LOG = Logger.getLogger(AddTagServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String tags = request.getParameter(TAG_NAMES_PARAM);

        try {
            if (tags != null)
                new TagService().addTag(tags);

            response.sendRedirect(TAGS_SERVLET_ADMIN);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
