package servlet.admin;

import models.Post;
import models.User;
import org.apache.log4j.Logger;
import services.PostService;
import servlet.LoginServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.ExploreServlet.CURRENT_USER_ATTR;
import static servlet.admin.BanUserServlet.USER_ID_PARAM;
import static servlet.admin.TagDeleteServlet.TAG_ID_PARAM;

@WebServlet(urlPatterns = {"/posts"})
public class PostsServlet extends HttpServlet {
    public static final String POSTS_SERVLET_ADMIN = "posts";
    private static final Logger LOG = Logger.getLogger(PostsServlet.class);
    private static final String POSTS_ATTR = "posts";
    private static final String POSTS_JSP_ADMIN = "jsp/admin/posts.jsp";


    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User currentUser = (User) request.getSession().getAttribute(LoginServlet.USER_SESSION_ATTR);
            String userIdParam = request.getParameter(USER_ID_PARAM);
            String tagIdParam = request.getParameter(TAG_ID_PARAM);
            List<Post> posts;
            PostService postService = new PostService();
            if (userIdParam != null) {
                int userId = Integer.parseInt(userIdParam);
                posts = postService.getPublishedOwnerPosts(userId);
            } else if (tagIdParam != null) {
                int tagId = Integer.parseInt(tagIdParam);
                posts = postService.getPublishedPostsByTag(tagId);
            } else
                posts = postService.getPublishedPosts();

            request.setAttribute(POSTS_ATTR, posts);
            request.setAttribute(CURRENT_USER_ATTR, currentUser);
            request.getRequestDispatcher(POSTS_JSP_ADMIN).forward(request, response);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
