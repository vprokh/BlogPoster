package servlet.admin;

import models.Tag;
import models.User;
import org.apache.log4j.Logger;
import services.TagService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static servlet.ExploreServlet.CURRENT_USER_ATTR;
import static servlet.LoginServlet.USER_SESSION_ATTR;

@WebServlet(urlPatterns = {"/tags"})
public class TagsServlet extends HttpServlet {
    public static final String TAGS_SERVLET_ADMIN = "/tags";
    private static final String TAGS_ATTR = "tags";
    private static final Logger LOG = Logger.getLogger(TagsServlet.class);
    private static final String TAGS_JSP = "jsp/admin/tags.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User currentUser = (User) request.getSession().getAttribute(USER_SESSION_ATTR);
            Set<Tag> tags = new TagService().getTagSet();
            request.setAttribute(TAGS_ATTR, tags);
            request.setAttribute(CURRENT_USER_ATTR, currentUser);
            request.getRequestDispatcher(TAGS_JSP).forward(request, response);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
