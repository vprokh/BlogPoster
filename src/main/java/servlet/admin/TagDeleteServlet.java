package servlet.admin;

import org.apache.log4j.Logger;
import services.TagService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.admin.TagsServlet.TAGS_SERVLET_ADMIN;

@WebServlet(urlPatterns = {"/deleteTag"})
public class TagDeleteServlet extends HttpServlet {
    public static final String DELETE_TAG_SERVLET_ADMIN = "deleteTag";
    public static final String TAG_ID_PARAM = "tagId";
    private static final Logger LOG = Logger.getLogger(TagDeleteServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            int tagId = Integer.parseInt(request.getParameter(TAG_ID_PARAM));
            new TagService().deleteTag(tagId);
            response.sendRedirect(TAGS_SERVLET_ADMIN);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
