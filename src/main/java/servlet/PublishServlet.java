package servlet;

import org.apache.log4j.Logger;
import services.PostService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.SinglePostServlet.POST_ID_PARAM;
import static servlet.UserPostServlet.POST_TYPE_PARAM;
import static servlet.UserPostServlet.USER_POSTS_SERVLET;

@WebServlet(urlPatterns = {"/publish"})
public class PublishServlet extends HttpServlet {
    public static final String PUBLISH_POST_SERVLET = "publish";
    private static final Logger LOG = Logger.getLogger(PublishServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int postId = Integer.parseInt(request.getParameter(POST_ID_PARAM));
        String postType = request.getParameter(POST_TYPE_PARAM);

        try {
            new PostService().publishPostById(postId);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        response.sendRedirect(USER_POSTS_SERVLET + "?" + POST_TYPE_PARAM + "=" + postType);
    }
}
