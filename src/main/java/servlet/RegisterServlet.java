package servlet;

import encryption.BCrypter;
import org.apache.log4j.Logger;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.ExploreServlet.EXPLORE_SERVLET;
import static servlet.LoginServlet.SESSION_TIME_OUT;
import static servlet.LoginServlet.USER_SESSION_ATTR;

@WebServlet(urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {
    private static final String USER_NAME = "username";
    private static final String USER_LAST_NAME = "lastName";
    private static final String USER_EMAIL = "userEmail";
    private static final String PASSWORD = "userPassword";
    private static final String CONFIRM_PASSWORD = "confirmUserPassword";

    static final String REGISTER_ERRORS = "registerError";

    private static final Logger LOG = Logger.getLogger(RegisterServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter(USER_NAME);
        String userLastName = request.getParameter(USER_LAST_NAME);
        String userEmail = request.getParameter(USER_EMAIL);
        String userPass = request.getParameter(PASSWORD);
        String userConfirmPass = request.getParameter(CONFIRM_PASSWORD);
        List<String> errors;
        try {
            errors = new UserService().registerUser(userName, userLastName, userEmail, userPass, userConfirmPass);
            if(!errors.isEmpty()) {
                request.setAttribute(REGISTER_ERRORS, errors);
                request.getRequestDispatcher(EXPLORE_SERVLET).forward(request, response);
                return;
            }
            HttpSession session = request.getSession();
            session.setAttribute(USER_SESSION_ATTR, new UserService().getUserByEmail(userEmail));
            session.setMaxInactiveInterval(SESSION_TIME_OUT);
            response.sendRedirect(EXPLORE_SERVLET);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(EXPLORE_SERVLET);
    }
}
