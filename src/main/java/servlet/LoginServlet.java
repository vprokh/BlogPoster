package servlet;

import org.apache.log4j.Logger;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.ExploreServlet.EXPLORE_SERVLET;
import static servlet.admin.UsersServlet.USERS_SERVLET_ADMIN;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private static final String USERNAME = "userEmail";
    private static final String PASSWORD = "userPassword";
    private Logger LOG = Logger.getLogger(LoginServlet.class);

    static final String LOGIN_ERROR_KEY = "loginError";
    static final int SESSION_TIME_OUT = 20*60;
    public static final String USER_SESSION_ATTR = "currentUser";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = new UserService();

        String email = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);
        String loginError;

        try {
            loginError = userService.loginUserByPassword(email, password);

            if (loginError != null) {
                request.setAttribute(LOGIN_ERROR_KEY, loginError);
                request.getRequestDispatcher(EXPLORE_SERVLET).forward(request, response);
            } else if (userService.getUserByEmail(email).isBlocked()) {
                response.sendRedirect("pageNotFound");
            } else if (userService.isRoleUser(email)) {
                HttpSession session = request.getSession();
                session.setAttribute(USER_SESSION_ATTR, new UserService().getUserByEmail(email));
                session.setMaxInactiveInterval(SESSION_TIME_OUT);
                response.sendRedirect(EXPLORE_SERVLET);
            } else if (userService.isUserAdmin(email)){
                HttpSession session = request.getSession();
                session.setAttribute(USER_SESSION_ATTR, new UserService().getUserByEmail(email));
                session.setMaxInactiveInterval(SESSION_TIME_OUT);
                response.sendRedirect(USERS_SERVLET_ADMIN);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    }
}
