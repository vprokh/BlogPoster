package services;

import dao.PostDao;
import modelValidation.BlogValidation;
import models.Post;
import models.Tag;
import models.User;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

public class PostService {
    private static final int POST_OWNER_ID = 1;

    public List<Post> getPublishedPosts(int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;

        return new PostDao().getLimitExplorePost(postPerPage, offset);
    }

    public int getPublishedPostAmount() throws SQLException {
        List<Post> posts = new PostDao().getAllPublishedPosts();

        if(posts.isEmpty())
            return 0;

        return posts.size();
    }

    public List<String> addPost(String title, String description, String content, String imageSrc, Set<Tag> tags, List<User> collaborators, boolean isPublished) throws SQLException {
        LocalDate createDate = LocalDate.now();
        LocalDate publishingDate = null;
        LocalDate updateDate = null;
        LocalTime updateTime = null;

        if (isPublished) {
            publishingDate = createDate;
            updateDate = createDate;
            updateTime = LocalTime.now();
        }

        Post post = new Post(title, description, imageSrc, content, createDate, updateDate, publishingDate, updateTime, tags, collaborators);
        List<String> errors = new BlogValidation().validate(post);
        System.out.println("errors = " + errors);
        if (!errors.isEmpty())
            return errors;

        new PostDao().addPost(post);

        return null;
    }

    /* for size */
    public int getPublishedOwnerPostsAmount(int userId) throws SQLException {
        List<Post> published = new PostDao().getPublishedPostsById(userId, POST_OWNER_ID);

        if (published != null)
            return published.size();
        return 0;
    }

    public int getOwnerDraftAmount(int userId) throws SQLException {
        List<Post> drafts = new PostDao().getDraftPostsById(userId, POST_OWNER_ID);

        if (drafts != null)
            return drafts.size();
        return 0;
    }

    public int getCollaboratingPostsAmount(int userId) throws SQLException {
        List<Post> collaborations = new PostDao().getCollabPostsById(userId, POST_OWNER_ID);

        if (collaborations != null)
            return collaborations.size();
        return 0;
    }

    /* limit for pagination */
    public List<Post> getPublishedOwnerPosts(int userId, int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;
        return new PostDao().getPublishedPostsById(userId, POST_OWNER_ID, postPerPage, offset);
    }

    public List<Post> getPublishedOwnerPosts(int userId) throws SQLException {
        return new PostDao().getPublishedPostsById(userId, POST_OWNER_ID);
    }

    public List<Post> getOwnerDrafts(int userId, int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;
        return new PostDao().getDraftPostsById(userId, POST_OWNER_ID, postPerPage, offset);
    }

    public List<Post> getCollaboratingPosts(int userId, int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;
        return new PostDao().getCollaborationPostsById(userId, POST_OWNER_ID, postPerPage, offset);
    }

    /* for search*/
    public int getVisiblePostsAmount(int userId) throws SQLException {
        List<Post> visible = new PostDao().getVisiblePostsById(userId);

        if (visible != null)
            return visible.size();

        return 0;
    }

    public List<Post> getVisiblePostsByUserId(int userId, int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;

        return new PostDao().getVisiblePostsById(userId, postPerPage, offset);
    }

    public List<Post> getPublishedPostsByTagId(int tagId, int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;

        return new PostDao().getPublishedPostsByTagId(tagId, postPerPage, offset);
    }

    public int getPublishedPostsByTagIdAmount(int id) throws SQLException {
        return new PostDao().getPublishedPostsByTagId(id).size();
    }

    public void deletePostById(int postId) throws SQLException {
        new PostDao().deletePostById(postId);
    }

    public void publishPostById(int postId) throws SQLException {
        LocalDate publishDate = LocalDate.now();
        LocalTime updateTime = LocalTime.now();

        new PostDao().updatePostPublishDate(postId, publishDate, publishDate, updateTime);
    }

    public Post getPostById(int postId) throws SQLException {
        return new PostDao().getPostById(postId);
    }

    public List<String> updatePost(int postId, String title, String description, String content, String imageSrc, List<User> users, Set<Tag> tagSet) throws SQLException {
        LocalDate updateDate = LocalDate.now();
        LocalTime updateTime = LocalTime.now();
        PostDao postDao = new PostDao();

        Post post = postDao.getPostById(postId);
        System.out.println("Post to update = " + post.toString());
        System.out.println("First date of create = " + post.getDateOfCreate().toString());

        post.setDateOfUpdate(updateDate);
        post.setTimeOfUpdate(updateTime);

        if (title != null)
            post.setTitle(title);
        if (description != null)
            post.setDescription(description);
        if (content != null)
            post.setContent(content);
        if (imageSrc != null)
            post.setImageUrl(imageSrc);
        if (tagSet != null)
            post.setTagList(tagSet);
        if (users != null)
            post.setUsers(users);



        System.out.println("Post to update = " + post);
        List<String> errors = new BlogValidation().validate(post);

        System.out.println("errors = " + errors);

        if(!errors.isEmpty()) {
            return errors;
        }

        new PostDao().updatePostById(post);

        return null;
    }

    public List<String> updateCollabPost(int postId, String title, String description, String content, String imageSrc, Set<Tag> tagSet) throws SQLException {
        LocalDate updateDate = LocalDate.now();
        LocalTime updateTime = LocalTime.now();
        Post post = getPostById(postId);
        System.out.println("First date of create = " + post.getDateOfCreate().toString());
        post.setDateOfUpdate(updateDate);
        post.setTimeOfUpdate(updateTime);

        if (title != null)
            post.setTitle(title);
        if (description != null)
            post.setDescription(description);
        if (content != null)
            post.setContent(content);
        if (imageSrc != null)
            post.setImageUrl(imageSrc);
        if (tagSet != null)
            post.setTagList(tagSet);

        List<String> errors = new BlogValidation().validate(post);

        if(!errors.isEmpty()) {
            return errors;
        }

        new PostDao().updatePostById(post);

        return null;
    }

    public int getSearchedPostsAmount(String postTitlePart) throws SQLException {

        List<Post> searchedPosts = new PostDao().getSearchedPosts(postTitlePart.toLowerCase());
        if (searchedPosts == null)
            return 0;

        return searchedPosts.size();
    }

    public List<Post> getSearchedPosts(String postTitlePart, int currentPage, int postPerPage) throws SQLException {
        int offset = currentPage * postPerPage - postPerPage;

        return new PostDao().getSearchedPosts(postTitlePart.toLowerCase(), postPerPage, offset);
    }

    public List<Post> getPublishedPosts() throws SQLException {
        return new PostDao().getAllPublishedPosts();
    }

    public List<Post> getPublishedPostsByTag(int tagId) throws SQLException {
        return new PostDao().getPublishedPostsByTagId(tagId);
    }
}
