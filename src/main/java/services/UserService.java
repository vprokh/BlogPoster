package services;

import dao.UserDao;
import encryption.BCrypter;
import modelValidation.BlogValidation;
import models.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserService {
    private static final String USER_ALREADY_EXIST_MESSAGE = "User with this email already exist!";
    private static final String LOGIN_ERROR_MESSAGE = "Either email or password is incorrect!";
    private static final String PASSWORDS_NOT_EQUAL = "Passwords are not equal!";

    public List<String> registerUser(String name, String lastName, String email, String pass, String confirmPass) throws SQLException {
        List<String> errorMessages = new ArrayList<>();

        if (!pass.equals(confirmPass)) {
            errorMessages.add(PASSWORDS_NOT_EQUAL);
            return errorMessages;
        }

        if (getUserFromDB(email) != null) {
            errorMessages.add(USER_ALREADY_EXIST_MESSAGE);
            return errorMessages;
        }

        User user = new User();
        user.setName(name);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(BCrypter.getHashPassword(pass));

        errorMessages = new BlogValidation().validate(user);

        if(errorMessages.isEmpty())
            new UserDao().addUser(user);

        return errorMessages;
    }

    public String loginUserByPassword(String email, String password) throws SQLException {
        User user = new UserDao().getUserByEmail(email);
        if(user == null || BCrypter.checkPassword(password, user.getPassword()))
            return LOGIN_ERROR_MESSAGE;

        return null;
    }

    private User getUserFromDB(String email) throws SQLException {
        return new UserDao().getUserByEmail(email);
    }

    public boolean isUserAdmin(String email) throws SQLException {
        User user = getUserFromDB(email);
        return user.getRole().isRoleAdmin();
    }

    public User getUserByEmail(String email) throws SQLException {
        return new UserDao().getUserByEmail(email);
    }

    public List<User> getUsersByEmail(User owner, String[] emails) throws SQLException {
        List<User> users = new ArrayList<>();
        users.add(owner);
        User user;
        if (emails != null) {
            for (String email : emails) {
                user = getUserByEmail(email);
                if (user != null) {
                    users.add(user);
                }
            }
        }

        return users;
    }

    public List<String> updateGeneralInfo(User currentUser, String name, String lastName, String email) throws SQLException {
        currentUser.setName(name);
        currentUser.setLastName(lastName);
        currentUser.setEmail(email);

        List<String> errors = new BlogValidation().validate(currentUser);
        if (!errors.isEmpty())
            return errors;

        new UserDao().updateUserById(currentUser);

        return null;
    }

    public List<String> updateSecurityInfo(User currentUser, String newPassword, String confirmNew, String currentPassword) throws SQLException {
        List<String> errors = new ArrayList<>();

        if (!newPassword.equals(confirmNew)) {
            errors.add("Passwords do not match!!!");
            return errors;
        } else if (BCrypter.checkPassword(currentPassword, currentUser.getPassword())) {
            errors.add("Current password is wrong!");
            return errors;
        }

        currentUser.setPassword(BCrypter.getHashPassword(newPassword));
        errors = new BlogValidation().validate(currentUser);

        if (!errors.isEmpty())
            return errors;

        new UserDao().updateUserById(currentUser);

        return null;
    }

    public List<String> deleteUserAccount(User currentUser, String currentPassword, String confirmCurrentPassword) throws SQLException {
        List<String> errors = new ArrayList<>();

        if (BCrypter.checkPassword(currentPassword, currentUser.getPassword())) {
            errors.add("Current password is wrong!");
            return errors;
        } else if (!currentPassword.equals(confirmCurrentPassword)) {
            errors.add("Passwords do not match!!!");
            return errors;
        }

        new UserDao().deleteUserById(currentUser.getUserId());

        return null;
    }

    public List<User> getAllUsers(int currentUserId) throws SQLException {
        List<User> users = new UserDao().getAllUser();
        if (users == null || users.isEmpty())
            return null;

        return users.stream().filter(user -> user.getUserId() != currentUserId).collect(Collectors.toList());

    }

    public void banUserById(int userId, boolean ban) throws SQLException {
        new UserDao().banUserById(userId, ban);
    }

    public boolean isRoleUser(String email) throws SQLException {
        User user = getUserFromDB(email);

        return user.getRole().isRoleUser();
    }
}
