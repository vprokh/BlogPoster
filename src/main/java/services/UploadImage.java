package services;

import java.io.*;

public class UploadImage {

    private UploadImage() {throw new IllegalStateException(UploadImage.class.getSimpleName()); }

    public static String uploadImage(String fileName, InputStream inputStream, String pathToUpload) throws IOException {
        if (!fileName.equals("")) {
            File uploadDir = new File(pathToUpload);

            if(!uploadDir.exists())
                if(!uploadDir.mkdir())
                    System.out.println("Cannot make dir!");

            String imgSrc = uploadDir + File.separator + fileName;
            System.out.println("img src = " + imgSrc);
            byte[] buffer = new byte[inputStream.available()];
            try (FileOutputStream fileOutputStream = new FileOutputStream(imgSrc)) {
                inputStream.read(buffer);
                fileOutputStream.write(buffer);
            }

            inputStream.close();

            return ReadFromProperty.getReadImageDirPath() + fileName;
        }
        return null;
    }
}
