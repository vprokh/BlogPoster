package services;

public class PostPagination {
    private static final int START_PAGE_NUMBER = 1;
    private PostPagination() { throw new IllegalStateException(PostPagination.class.getSimpleName());}

    public static int getCurrentPage(String currentPageParam) {
        if (currentPageParam != null)
            return Integer.valueOf(currentPageParam);
        return START_PAGE_NUMBER;
    }

    public static int getPagesAmount(int postAmount,int postPerPage) {
        return (int) Math.ceil(postAmount / (double) postPerPage);
    }
}
