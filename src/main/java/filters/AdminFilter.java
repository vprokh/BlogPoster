package filters;

import models.User;
import org.apache.log4j.Logger;
import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.admin.AddTagServlet.ADD_TAG_SERVLET_ADMIN;
import static servlet.admin.BanUserServlet.BAN_USER_SERVLET_ADMIN;
import static servlet.admin.PostsServlet.POSTS_SERVLET_ADMIN;
import static servlet.admin.TagDeleteServlet.DELETE_TAG_SERVLET_ADMIN;
import static servlet.admin.TagsServlet.TAGS_SERVLET_ADMIN;
import static servlet.admin.UsersServlet.USERS_SERVLET_ADMIN;

@WebFilter(urlPatterns = {'/'+POSTS_SERVLET_ADMIN, '/'+USERS_SERVLET_ADMIN, '/'+TAGS_SERVLET_ADMIN,
        '/'+DELETE_TAG_SERVLET_ADMIN, '/'+ADD_TAG_SERVLET_ADMIN, '/'+BAN_USER_SERVLET_ADMIN})
public class AdminFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(AdminFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpServletRequest.getSession();

        User user = (User) session.getAttribute(USER_SESSION_ATTR);

        try {
            if (user != null && new UserService().isUserAdmin(user.getEmail())) {
                chain.doFilter(request, response);
            } else {
                httpResponse.sendRedirect("pageNotFound");
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
    @Override
    public void destroy() {

    }
}