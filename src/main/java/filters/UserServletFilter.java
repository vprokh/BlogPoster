package filters;

import models.User;
import org.apache.log4j.Logger;
import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.AddPostServlet.CREATE_POST_SERVLET;
import static servlet.ChangeUserServlet.CHANGE_USER_SERVLET;
import static servlet.EditPostServlet.EDIT_POST_SERVLET;
import static servlet.ExploreServlet.EXPLORE_SERVLET;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.ProfileServlet.PROFILE_SERVLET;
import static servlet.PublishServlet.PUBLISH_POST_SERVLET;
import static servlet.UserPostServlet.USER_POSTS_SERVLET;

@WebFilter(urlPatterns = {'/'+CREATE_POST_SERVLET, '/'+CHANGE_USER_SERVLET, '/'+EDIT_POST_SERVLET,
        '/'+EXPLORE_SERVLET, '/'+PROFILE_SERVLET, '/'+PUBLISH_POST_SERVLET, '/'+USER_POSTS_SERVLET})
public class UserServletFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(UserServletFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        User currentUser = (User) httpServletRequest.getSession().getAttribute(USER_SESSION_ATTR);

        try {
            if (currentUser != null && new UserService().isUserAdmin(currentUser.getEmail())) {
                httpResponse.sendRedirect("pageNotFound");
            } else {
                chain.doFilter(request, response);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
    @Override
    public void destroy() {

    }
}