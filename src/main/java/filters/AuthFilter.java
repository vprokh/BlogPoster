package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static servlet.AddPostServlet.CREATE_POST_SERVLET;
import static servlet.ChangeUserServlet.CHANGE_USER_SERVLET;
import static servlet.DeletePostServlet.DELETE_POST_SERVLET;
import static servlet.EditPostServlet.EDIT_POST_SERVLET;
import static servlet.ExploreServlet.EXPLORE_SERVLET;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.LogoutServlet.LOGOUT_SERVLET;
import static servlet.ProfileServlet.PROFILE_SERVLET;
import static servlet.PublishServlet.PUBLISH_POST_SERVLET;
import static servlet.UserPostServlet.USER_POSTS_SERVLET;

@WebFilter(urlPatterns = {'/'+CREATE_POST_SERVLET, '/'+CHANGE_USER_SERVLET, '/'+DELETE_POST_SERVLET, '/'+EDIT_POST_SERVLET,
        '/'+LOGOUT_SERVLET, '/'+PROFILE_SERVLET, '/'+PUBLISH_POST_SERVLET, '/'+USER_POSTS_SERVLET})
public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();

        if(session == null || session.getAttribute(USER_SESSION_ATTR) == null) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.sendRedirect(EXPLORE_SERVLET);
        } else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
