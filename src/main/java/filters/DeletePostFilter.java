package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.DeletePostServlet.DELETE_POST_SERVLET;
import static servlet.SinglePostServlet.POST_ID_PARAM;

@WebFilter(urlPatterns = {'/'+DELETE_POST_SERVLET})
public class DeletePostFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String postId = httpServletRequest.getParameter(POST_ID_PARAM);

        if (postId == null) {
            httpResponse.sendRedirect("pageNotFound");
        } else {
            chain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {

    }
}