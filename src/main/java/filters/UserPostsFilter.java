package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.UserPostServlet.POST_TYPE_PARAM;
import static servlet.UserPostServlet.USER_POSTS_SERVLET;

@WebFilter(urlPatterns = {'/'+USER_POSTS_SERVLET})
public class UserPostsFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String postType = httpServletRequest.getParameter(POST_TYPE_PARAM);

        if (postType == null) {
            httpResponse.sendRedirect("pageNotFound");
        } else {
            chain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {

    }
}