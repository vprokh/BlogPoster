package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

import static servlet.AddPostServlet.POST_TITLE_PARAM;
import static servlet.ExploreServlet.EXPLORE_SERVLET;
import static servlet.SearchServlet.SEARCH_ID_PARAM;
import static servlet.SearchServlet.SEARCH_SERVLET;
import static servlet.SearchServlet.SEARCH_TYPE_PARAM;
import static servlet.UserPostServlet.POST_TYPE_PARAM;
import static servlet.UserPostServlet.USER_POSTS_SERVLET;

@WebFilter(urlPatterns = {'/'+SEARCH_SERVLET, '/'+EXPLORE_SERVLET, '/'+USER_POSTS_SERVLET})
public class PaginationUrlFilter implements Filter {
    private static final String PAGINATION_URL_ATTR = "paginationUrl";

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String searchType = request.getParameter(SEARCH_TYPE_PARAM);
        String postType = request.getParameter(POST_TYPE_PARAM);
        String postTitle = request.getParameter(POST_TITLE_PARAM);
        String url;

        if (postTitle != null) {
            url = SEARCH_SERVLET + "?" + POST_TITLE_PARAM + "=" + postTitle + "&";
        } else if (searchType != null) {
            int id = Integer.parseInt(request.getParameter(SEARCH_ID_PARAM));
            url = SEARCH_SERVLET + "?" + SEARCH_TYPE_PARAM + "=" + searchType + "&" + SEARCH_ID_PARAM + "=" + id + "&";
        } else if (postType != null) {
            url = USER_POSTS_SERVLET + "?" + POST_TYPE_PARAM + "=" + postType + "&";
        } else
            url = EXPLORE_SERVLET + "?";
        request.setAttribute(PAGINATION_URL_ATTR, url);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
