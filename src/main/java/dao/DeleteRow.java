package dao;

import dataBase.DataBaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteRow{
    public static void deleteRowByIdAndRequest(int id, String dropTagByIdRequest) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(dropTagByIdRequest);

        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }
}
