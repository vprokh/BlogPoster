package dao.interfaces;

import models.User;

import java.sql.SQLException;
import java.util.List;

public interface IDaoUser {
    User getUserById(int id) throws SQLException;
    void addUser(User user) throws SQLException;
    void deleteUserById(int id) throws SQLException;
    void updateUserById(User user) throws SQLException;
    List<User> getAllUser() throws SQLException;

    User getUserByEmail(String email) throws SQLException;

    void banUserById(int userId, boolean ban) throws SQLException;
}
