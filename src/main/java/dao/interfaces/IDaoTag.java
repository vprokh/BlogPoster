package dao.interfaces;

import models.Tag;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public interface IDaoTag {
    Tag getTagById(int id) throws SQLException;
    void addTag(String tagName) throws SQLException;
    void deleteTagById(int id) throws SQLException;
    void updateTagById(Tag tag) throws SQLException;
    Set<Tag> getAllTag() throws SQLException;

    Tag getTagByName(String name) throws SQLException;

    List<String> getTagNames() throws SQLException;
}
