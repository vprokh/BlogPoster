package dao;

import dao.interfaces.IDaoUser;
import dataBase.DataBaseConnection;
import models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserDao implements IDaoUser {
    /* user table column names */
    private static final String USER_ID_COLUMN_NAME = "user_id";
    private static final String USER_NAME_COLUMN_NAME = "name";
    private static final String USER_LAST_NAME_COLUMN_NAME = "last_name";
    private static final String USER_EMAIL_COLUMN_NAME = "email";
    private static final String USER_PASSWORD_COLUMN_NAME = "password";
    private static final String USER_SOCIAL_URL_COLUMN_NAME = "social_url";
    private static final String USER_IS_BLOCKED_COLUMN_NAME = "is_blocked";
    private static final String USER_ROLE_ID_COLUMN_NAME = "role_id";

    /* SQL requests */
    private static final String GET_USER_BY_ID_REQUEST = "SELECT * FROM public.user WHERE " + USER_ID_COLUMN_NAME + "=?";
    private static final String ADD_USER_REQUEST = "INSERT INTO public.user(" +
            USER_NAME_COLUMN_NAME + ", " + USER_LAST_NAME_COLUMN_NAME + ", " +
            USER_EMAIL_COLUMN_NAME + ", " + USER_PASSWORD_COLUMN_NAME + ", " +
            USER_SOCIAL_URL_COLUMN_NAME + ", " + USER_IS_BLOCKED_COLUMN_NAME + ", " +
            USER_ROLE_ID_COLUMN_NAME +
            ") VALUES(?, ?, ?, ?, ?, ?, ?)";
    private static final String DROP_USER_BY_ID_REQUEST = "DELETE FROM public.user WHERE " + USER_ID_COLUMN_NAME + "=?";
    private static final String UPDATE_USER_BY_ID_REQUEST = "UPDATE public.user SET " +
            USER_NAME_COLUMN_NAME + "=?, " + USER_LAST_NAME_COLUMN_NAME + "=?, " +
            USER_EMAIL_COLUMN_NAME + "=?, " + USER_PASSWORD_COLUMN_NAME + "=?, " +
            USER_SOCIAL_URL_COLUMN_NAME + "=?, " + USER_IS_BLOCKED_COLUMN_NAME + "=?, " +
            USER_ROLE_ID_COLUMN_NAME + "=? " +
            "WHERE user_id=?";
    private static final String GET_ALL_USER_REQUEST = "SELECT * FROM public.user";
    private static final String GET_USER_BY_EMAIL = "SELECT * FROM public.user WHERE " + USER_EMAIL_COLUMN_NAME + "=?";
    private static final String BAN_USER_BY_ID = "UPDATE public.user SET " +
            USER_IS_BLOCKED_COLUMN_NAME + "=? WHERE user_id=?";

    @Override
    public User getUserById(int id) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_ID_REQUEST);

        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        User user = getUser(resultSet);
        resultSet.close();
        preparedStatement.close();
        connection.close();

        return user;
    }

    @Override
    public void addUser(User user) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(ADD_USER_REQUEST);
        
        preparedStatement.setString(1, user.getName());
        preparedStatement.setString(2, user.getLastName());
        preparedStatement.setString(3, user.getEmail());
        preparedStatement.setString(4, user.getPassword());
        preparedStatement.setString(5, user.getSocialUrl());
        preparedStatement.setBoolean(6, user.isBlocked());
        preparedStatement.setInt(7, user.getRole().getRoleId());

        preparedStatement.executeUpdate();
        preparedStatement.close();
        connection.close();
    }

    @Override
    public void deleteUserById(int id) throws SQLException {
        DeleteRow.deleteRowByIdAndRequest(id, DROP_USER_BY_ID_REQUEST);
    }

    @Override
    public void updateUserById(User user) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_BY_ID_REQUEST);

        preparedStatement.setString(1, user.getName());
        preparedStatement.setString(2, user.getLastName());
        preparedStatement.setString(3, user.getEmail());
        preparedStatement.setString(4, user.getPassword());
        preparedStatement.setString(5, user.getSocialUrl());
        preparedStatement.setBoolean(6, user.isBlocked());
        preparedStatement.setInt(7, user.getRole().getRoleId());
        preparedStatement.setInt(8, user.getUserId());
        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }

    @Override
    public List<User> getAllUser() throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USER_REQUEST);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<User> users = new ArrayList<>();

        while(resultSet.next()) {
            users.add(getUserById(resultSet.getInt(USER_ID_COLUMN_NAME)));
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if (users.isEmpty())
            return null;

        return users;
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_EMAIL);
        User user;

        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();
        user = getUser(resultSet);

        resultSet.close();
        preparedStatement.close();
        connection.close();

        return user;
    }

    @Override
    public void banUserById(int userId, boolean ban) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(BAN_USER_BY_ID);

        preparedStatement.setBoolean(1, ban);
        preparedStatement.setInt(2,userId);

        preparedStatement.executeUpdate();
        preparedStatement.close();
        connection.close();
    }

    private User getUser(ResultSet resultSet) throws SQLException {
        User user = null;

        if(resultSet.next()) {
            user = new User();
            user.setUserId(resultSet.getInt(USER_ID_COLUMN_NAME));
            user.setName(resultSet.getString(USER_NAME_COLUMN_NAME));
            user.setLastName(resultSet.getString(USER_LAST_NAME_COLUMN_NAME));
            user.setEmail(resultSet.getString(USER_EMAIL_COLUMN_NAME));
            user.setPassword(resultSet.getString(USER_PASSWORD_COLUMN_NAME));
            user.setSocialUrl(resultSet.getString(USER_SOCIAL_URL_COLUMN_NAME));
            user.setBlocked(resultSet.getBoolean(USER_IS_BLOCKED_COLUMN_NAME));
            user.setRole(new RoleDao().getRoleById(resultSet.getInt(USER_ROLE_ID_COLUMN_NAME)));
        }
        return user;
    }
}

