<div class="modal fade" id="actionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <c:if test="${errorMessage == null}">
                <div class="modal-header">
                    <h5 class="modal-title text-left">Are You sure?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <div class="modal-body text-center">
                ${actionMessage}
            </div>
            <div class="modal-footer">
                <c:choose>
                    <c:when test="${errorMessage == null}">
                        <a id="yesModalBtn" type="button" class="btn btn-primary">Yes</a>
                        <a type="button" class="btn btn-secondary" data-dismiss="modal">No</a>
                    </c:when>
                    <c:otherwise>
                        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>