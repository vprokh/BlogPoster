<div class="modal fade" id="successModal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg fixed-bottom">
        <div class="modal-content">
            <div class="modal-body text-center">
                <i class="fa fa-check"></i><b>${infoMessage}</b>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->
<script>
    $(document).ready(function () {
        var myModal = $('#successModal');
        myModal.modal('handleUpdate');
        myModal.modal('show');
        myModal.on({
            focus: function () {
                $(this).blur();
            }
        });
        myModal.on('shown.bs.modal', function () {
            clearTimeout(myModal.data('hideInteval'));
            var id = setTimeout(function(){
                myModal.modal('hide');
            }, 1250);
            myModal.data('hideInteval', id);
        })
    })
</script>