<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Posts | BlogPoster</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/headerStylesheet.css">
    <script src="../../jquery/jQuery3.1.1.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <style>
        .btn {
            width: 5rem;
            margin: 0.25rem;
        }
        .badge {
            margin: 0.2rem;
        }
    </style>
</head>
    <body>
    <%@include file="headerAdmin.jsp"%>
    <%@include file="../../html/confirmActionModal.html"%>
    <div class="container text-center w-100">
            <div class="col-md-12">
                <h4 style="margin-bottom: 1rem;">BlogPoster published posts</h4><br>
                <div class="table-responsive">
                    <table data-sort-name="id" class="table table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Image url</th>
                            <th>Date of create</th>
                            <th>Date of publish</th>
                            <th>Date of update</th>
                            <th>Time of update</th>
                            <th>Users</th>
                            <th>Tags</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${posts}" var="post">
                            <tr>
                                <td>${post.getPostId()}</td>
                                <td>${post.getTitle()}</td>
                                <td>${post.getImageUrl()}</td>
                                <td>${post.getDateOfCreate()}</td>
                                <td>${post.getDateOfPublishing()}</td>
                                <td>${post.getDateOfUpdate()}</td>
                                <td>${post.getTimeOfUpdate()}</td>
                                <td>
                                    <c:forEach items="${post.getUsers()}" var="user">
                                        <a href="posts?userId=${user.getUserId()}">${user.getName()} ${user.getLastName()}</a>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:forEach items="${post.getTagList()}" var="tag">
                                        <a href="posts?tagId=${tag.getTagId()}"><span class="badge badge-info">${tag.getName()}</span></a>
                                    </c:forEach>
                                </td>
                                <td><a href="post?postId=${post.getPostId()}" class="btn btn-success">Read</a><br>
                                    <a href="deletePost?postId=${post.getPostId()}" class="btn btn-danger" data-toggle="modal" data-target="#actionModal">Delete</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    <script>
        $('.btn').click(function () {
            var currentHref = $(this).attr('href');
            console.log("current href = " + currentHref);

            $('#yesModalBtn').attr('href', currentHref);
        });
    </script>
    </body>
</html>
