<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Single | BlogPoster</title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../../bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../css/singlePost.css" rel="stylesheet">
    <link href="../../css/headerStylesheet.css" rel="stylesheet">
    <script src="../../jquery/jQuery3.1.1.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>

    <style>
        .header {
            background: #4DFAD2;
            width: 100%;
            background: linear-gradient(
                    to bottom,
                    rgba(77, 250, 210, 0.6),
                    rgba(127, 250, 221, 0.5)
            ) ,url('${post.getImageUrl()}') no-repeat left top;
            background-size: cover;
            z-index: 0;
        }
        .btn {
            width: 5rem;
            margin: 0.1rem;
        }
    </style>
</head>
<body>
<%@include file="headerAdmin.jsp"%>
<%@include file="../../html/confirmActionModal.html"%>
    <div class="header">
        <div class="text-center">
            <div class="caption"><br><br><br>
                <h2 class="title display-3">${post.getTitle()}</h2>
                <h4>
                    <i class="fa fa-user"></i>
                    <c:forEach items="${post.getUsers()}" var="user">
                        <a href="posts?userId=${user.getUserId()}">${user.getName()} ${user.getLastName()}</a>
                    </c:forEach>
                    <hr>
                    <i class="fa fa-calendar"></i> Posted on ${post.getDateOfPublishing()}
                    <c:if test="${post.getDateOfUpdate() != null && post.getDateOfUpdate() != post.getDateOfPublishing()}"> <i>(Updated: ${post.getDateOfUpdate()} on ${post.getTimeOfUpdate()}) </i> </c:if>
                    <hr><br>
                    <i class="fa fa-tags"></i> Tags:
                    <c:forEach items="${post.getTagList()}" var="tag"> <a href="posts?tagId=${tag.getTagId()}">
                        <span class="badge badge-info">${tag.getName()}</span></a>
                    </c:forEach>
                </h4>
            </div>
        </div>
    </div>
    <div class="container w-75">
        <div class="card mb-4">
            <div class="card-body text-center">
                    <h3>Post description</h3>
                <h4>${post.getDescription()}</h4>
                    <h3>Post Content</h3>
                <h4>${post.getContent()}</h4>
            </div>
        </div>
        <a href="posts" class="btn btn-primary">Back</a>
        <a href="deletePost&postId=${post.getPostId()}" class="btn btn-danger" data-toggle="modal" data-target="#actionModal">Delete</a>
    </div>

<script>
    $('.btn').click(function () {
        var currentHref = $(this).attr('href');
        console.log("current href = " + currentHref);

        $('#yesModalBtn').attr('href', currentHref);
    });
</script>
</body>
</html>
