<c:forEach items="${explorePost}" var="post">
    <h1 class="my-4">${post.getTitle()}</h1>
    <c:if test="${explorePost != null && post.getUsers().size() > 0}">
        <p class="lead"><i class="fa fa-user" aria-hidden="true"></i> by
            <c:forEach items="${post.getUsers()}" var="user"> <a href="search?searchType=user&id=${user.getUserId()}">${user.getName()} ${user.getLastName()}</a> </c:forEach>
        </p>
        <hr>
    </c:if>
    <p><i class="fa fa-calendar"></i> Posted on ${post.getDateOfPublishing()}
        <c:if test="${post.getDateOfUpdate() != null && post.getDateOfUpdate() != post.getDateOfPublishing()}"> <i>(Updated: ${post.getDateOfUpdate()} on ${post.getTimeOfUpdate()}) </i> </c:if>
    </p>
    <p><i class="fa fa-tags"></i> Tags:
        <c:forEach items="${post.getTagList()}" var="tag"> <a href="search?searchType=tag&id=${tag.getTagId()}"><span class="badge badge-info">${tag.getName()}</span></a>
        </c:forEach>
    </p>
    <hr>
    <div class="card mb-4">
        <img class="card-img-top" src="${post.getImageUrl()}" alt="Something went wrong">
        <div class="card-body">
            <p class="card-text">${post.getDescription()}</p>
            <a href="post?postId=${post.getPostId()}" class="btn btn-primary">Read More</a>
        </div>
    </div>
</c:forEach>
<c:if test="${explorePost == null || explorePost.size() == 0}">
    <div class="container w-100 tab-pane text-center">
        <div class="error-template mx-auto">
            <h3 style="color: darkgrey">
                Hmmm...
            </h3>
            <h4 style="color: darkgrey">
                Looks like there is no posts yet
            </h4>
            <div class="error-actions">
                <br>
                <a href="create" class="w-25 btn btn-primary"><span class="glyphicon glyphicon-home"></span>
                    Create post </a>
            </div>
        </div>
    </div>
</c:if>