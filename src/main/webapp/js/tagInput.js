var tagArray;

$(document).ready(function () {

    $('#addPost #editPost').on('keyup keypress', function(e) {
        console.log('enter pressed');
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    tagArray = $('.tagName').map(function(){
        return $.trim($(this).text());
    }).get();

});

$('.tagName').click(function () {
    var tagInput = $("#tags");
    tagInput.tagsinput('add', $(this).text());
    console.log(tagInput.val());
    $(this).hide("slow");
});


$('#tags').on('beforeItemAdd', function(event) {
    if (tagArray.indexOf(event.item) === -1)
        event.cancel = true;
});